import json, os, xlsxwriter # Importacion de Json, os para ejecutar funciones del sistema y xlsxwriter para generar archivos Excel
import nltk # Importando la libreria de nltk para el manejo de datos
import re # Importando re para utilizar expresiones regulares
from datetime import date # importando date para el manejo de fechas
from nltk.tokenize import word_tokenize # Importando la libreria para tokenizar las palabras

from dateutil.relativedelta import relativedelta # Importando la libreria para manejo de fechas

from means.cleaner import lower_case, empty_words, stemming, search_bad_word, count_bad_word, json_update
from means.qualification import formater_date, amount_date # Importando funciones para Formateo de Fechas y modificacion de fechas
from means.progressbar import print_background, print_fail ,print_pass, print_warn, print_info, print_bold, print_success, print_movement
import psycopg2, psycopg2.extras # Importando la libreria de postgres

conn = psycopg2.connect(database='txt_app',user='txt_user',password='XXXYYYZZZ', host='localhost') # Realizando la conexion con la base de datos de postgres 

# Funcion para escribir el jsons
def write_text(messages_list,file_name, contactName):    

	# Creando Estructura del json a escribir
	data = {}
	chain = []
	data['messages'] = []  
	data['name_file'] = None
	data['linear_chain'] = None
	data['classification'] = None

	with open('data/jsondb/̣' + str(file_name), 'w') as outfile: # Creacion de Archivos Json limpios/tokenizados/raiz en la carpeta jsondb

		sw = None

		for txt in messages_list: # Recibiendo diccionario messages del archivo json sin limpiar
			if 'remoteResource' in txt: # Preguntando si es una conversacion Grupal "Pues remoteResource es una propíedad que solo tiene los chats de 1 a 1"
				sw = True
			else:
				sw = False
			try:

				if txt['type'] == "text":# Seleccionando las conversaciones de tipo text
					if len(txt['text']) > 0:
						try:
							if sw:
								# Extrayendo el Remitente Numero de celular de conversaciones de 1 a 1
								remote_resource = str(txt['remoteResource'])
								_remote_resource = re.sub("\D", "", remote_resource)
							else:
								# Extrayendo el Remitente Numero de celular de conversaciones grupales
								_remote_resource = re.sub("\D", "", contactName)								


							timestamp = str(txt['timestamp']) # Extrayendo la fecha de la conversacion
							# Extrayendo el texto de la conversacions
							save_text = lower_case(nltk.word_tokenize(str(txt['text']))) # Tokenizamos la conversacion y la pasamos a las funciones para su limpieza

							# Preguntamos si el text obtenido de la limpieza no es vacio
							if len(save_text) > 0:
								for word in save_text:
									chain.append(word)

									if search_bad_word(word):
										try: 
											cur = conn.cursor() 
											# Insertando a base de datos las palabras malas
											cur.execute("INSERT INTO dates (msg,dt,name_conv) VALUES (%s,%s,%s)", (word, timestamp, file_name))
											conn.commit()
										except:
											pass

								# Insertando datos en las listas del json
								data['messages'].append({
									"telephone" : _remote_resource,
									"timestamp" : formater_date((timestamp[0:10])),
									"text_tokenize" : nltk.word_tokenize(str(txt['text'])),
									"text_cleaned" : stemming(save_text)
								})

								data['name_file'] = file_name
								data['linear_chain'] = chain

						except:
							pass
			except:
				pass

		# Guardando el json 
		json.dump(data, outfile)

	list_text_tokenize = []
	list_text_cleaned = []

	with open('data/jsondb/̣' + str(file_name), "r+") as jsonFile:
		data = json.load(jsonFile)
		for x in data['messages']:
			for d1 in x['text_tokenize']:
				list_text_tokenize.append(d1)
			for d in x['text_cleaned']:
				list_text_cleaned.append(d)				

	print("\n")
	print_info(str(file_name) + "\n")
	print_background()
	print_success("Texto Limpio ")
	print(list_text_cleaned,"\n")

	print_warn("Texto Sucio ")
	print(list_text_tokenize,"\n")



# Funcion para contar la cantidad de palabras malas por conversacion
def linear_paragraph(messages_list,file_name):    
	
	# Creando Estructura del json a escribir

	counter = 0
	chain = []
	_bad_word = []
	_dic_bar = []

	data_linear_paragraph = {}

	data_linear_paragraph['messages'] = []  

	data_linear_paragraph['counter_words'] = None

	data_linear_paragraph['bad_words'] = []

	# Creacion de Archivos Json en la carpeta jsonaqualification
	with open('data/jsonqualification/̣' + str(file_name), 'w') as outfile:

		for txt in messages_list:
			# extrayendo el mensaje ya limpio para contabilizar la cantidad de veces que se repite
			save_text = lower_case(nltk.word_tokenize(str(txt['text_cleaned'])))
			_save_text = stemming(save_text)

			for word in _save_text:
				# Verificando si la variable no es vacia
				if len(word) > 0:
					# Agregando las palabras a una lista llamada _bad_word
					chain.append(word)
					_bad_word.append(word)
			# Formateando los datos de la lista para tener datos unicos "Evitando asi duplicidad de palabras"
			_save_text_n = list(set(_bad_word))			

		# Iterando las palabras unicas para contabilizar las veces que se repite en la conversacion
		for word in _save_text_n:
			if search_bad_word(word):# Verificando si es una palabra mala
				cont = count_bad_word(word,_bad_word) # Contabilizando la cantidad de veces que se repite la palabra mala

				# Agregando el contador por palabra mala al json
				data_linear_paragraph['bad_words'].append({
					str(word) : {'count':cont}
				})

		counter = len(set(chain))
		# Agregando todos las palabras unicas al json
		data_linear_paragraph['messages'].append({
			'message':stemming(set(chain)),
		})

		# Agregando la cantidad de palabras en la conversacion
		data_linear_paragraph['counter_words'] = counter


		print("\n")
		print_info(str(file_name) + "\n")
		print_background()
		print_success("Cantidad de Palabras Malas ")
		print(counter,"\n")


		# Guardando el json
		json.dump(data_linear_paragraph, outfile)


# Funcion para verificar cantidad de registros cada 3 meses por conversacion
def verify_date(dt1,dt2, file_name):

	cur = conn.cursor()
	# Ejecutando consulta a BD de una fecha inicial a una fecha fin de una conversacion
	sql = "SELECT count(*) from dates WHERE dt >= %s and dt <= %s and name_conv = %s;"	

	data=[dt1,dt2,file_name]
	cur.execute(sql,data)	
	result = cur.fetchone()
	cont_bad =int(result[0])
	# Retornando la respuesta en la variable cont_bad
	return cont_bad


# Funcion para incrementar una fecha en 3 meses => 2017-02-14 - 2017-05-14
def generar_fecha(auxx):    
	
	dd = date(auxx.year, auxx.month, auxx.day) + relativedelta(months = 1)
	f1, f2 = amount_date(str(dd),3)
	aux = str(f2)
	auxx = f2
	# Retornamos Fecha inicial y Fecha final
	return (f1,f2)


# Funcion para generar Fechas para la busqueda de registros
def generate_dates(dt,sw):

	data_dates = {}
	data_dates['months'] = []

	for _count in range(30):
		if sw:
			d1, d2 = amount_date(dt, 3)
			data_dates['months'].append({
				str(_count) : {
					'fin':str(d2),
					'inicio':str(d1),
				}
			})

			sw = False
			aux = str(d2)
			auxx = d2

		else:
			v1 , v2 = generar_fecha(auxx)
			data_dates['months'].append({
				str(_count) : {
					'fin':str(v2),
					'inicio':str(v1),
				}
			})

			auxx = v2

	return data_dates


# Funcion para Guardar en archivos json los registros de cantidad de palabras malas cada 3 meses
def write_json_dates(messages_list,file_name, query_file_name):

	# Estructura del json
	data_qualification = {}

	data_qualification['month_words'] = []  
	data_qualification['total_counter'] = []  

	sw = True

	for txt in messages_list:
		# Extrayendo la fecha de conversacion
		cc = txt['timestamp']
		dict_date = generate_dates(txt['timestamp'],sw)
		break

	# Creando el json de palabras malas cada 3 meses por conversacion
	with open('data/jsondates/'+ str(file_name), 'w') as outfile:
		
		counter = 0
		iteration_counter = 0

		for v in dict_date['months']:
			for key,val in v.items():
				# Ejecutando la consulta
				# verify_date retorna un contador a partir de la fecha inicial y fecha fin de cada 3 meses
				_cont_query = verify_date(val.get('inicio'),val.get('fin'),str(query_file_name))

				if _cont_query > 0:
					# verificando si el datos es significavo
					counter +=int(_cont_query) # Formateando el registro a entero
					iteration_counter += 1

					# guardando la palabra mala y su contador en el archivo json
					data_qualification['month_words'].append({
						str(val.get('inicio')+ ' - '+val.get('fin')) : {'count':_cont_query}
					})

				data_qualification['total_counter'] = counter


		# Cafilicando la conversacion "Positiva=0 o Negativa =1"

		if iteration_counter == 0:
			json_update(file_name, 0)

		if iteration_counter >= 1:
			if counter >= 50:
				json_update(file_name, 1)
			elif counter < 50:
				json_update(file_name, 0)

		elif iteration_counter >= 2:
			if counter >= 75:
				json_update(file_name, 1)

		elif iteration_counter >= 3 :
			if counter >= 100:
				json_update(file_name, 1)

		elif iteration_counter >= 4:
			if counter >= 125:
				json_update(file_name, 1)

		elif iteration_counter >= 5:
			if counter >= 150:
				json_update(file_name, 1)

		elif iteration_counter >= 6:
			if counter >= 175:
				json_update(file_name, 1)

		elif iteration_counter >= 7:
			if counter >= 200:
				json_update(file_name, 1)												
				
		elif iteration_counter >= 8:
			if counter >= 225:
				json_update(file_name, 1)

		elif iteration_counter >= 9:
			if counter >= 250:
				json_update(file_name, 1)

		elif iteration_counter >= 10:
			if counter >= 275:
				json_update(file_name, 1)

		elif iteration_counter >= 11:
			if counter >= 300:
				json_update(file_name, 1)												

		elif iteration_counter >= 12:
			if counter >= 325:
				json_update(file_name, 1)

		elif iteration_counter >= 13:
			if counter >= 350:
				json_update(file_name, 1)

		elif iteration_counter >= 14:
			if counter >= 375:
				json_update(file_name, 1)

		elif iteration_counter >= 15:
			if counter >= 400:
				json_update(file_name, 1)

		elif iteration_counter >= 16:
			if counter >= 425:
				json_update(file_name, 1)

		elif iteration_counter >= 17:
			if counter >= 450:
				json_update(file_name, 1)

		elif iteration_counter >= 18:
			if counter >= 475:
				json_update(file_name, 1)																												

		# Guardando el Archivo json
		json.dump(data_qualification, outfile)



	with open('data/jsondb/' + str(file_name), "r+") as jsonFile:
		data = json.load(jsonFile)
		x = data['classification']

	print("\n")
	print_info(str(file_name) + "\n")
	print_background()
	if x == 0:
		print_success("Conversacion Positiva ")
	elif x == 1 :
		print_warn("Conversacion Negativa ")



# Funcion para unir todas las conversaciones a una linea
def json_merge(messages_list,file_name):    

	_chain = []

	for txt in messages_list:
		for x in txt['message']:
			_chain.append(str(x))

	return _chain


# Funcion para crear un registrar todos las conversaciones limpias en un json
def json_lineal(chain,file_name):

	# Estructura del json
	data_lineal = {}
	data_lineal['messages'] = []
	data_lineal['total_counter'] = []
	chain_tokenize = nltk.word_tokenize(chain)
	_chain_tokenize = (set(chain_tokenize))

	# Creando json 
	with open('data/jsonmatriz/̣' + str(file_name), 'w') as outfile:

		# Asignando valores al archivo json
		data_lineal['messages'].append({
			'message':stemming(_chain_tokenize),
		})

		data_lineal['total_counter'].append({
			'message':len(chain_tokenize),
		})

		# Guardando archivo json
		json.dump(data_lineal, outfile)


# Funcion para leer los registros del json con nombre matriz.json
def json_matriz(messages_list):

	list_matriz = []

	with open('data/jsonmatriz/̣' + 'matriz.json', "r+") as jsonFile:
		data = json.load(jsonFile)
		for x in data['messages']:
			for d in x['message']:
				list_matriz.append(d)

	return cont