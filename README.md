## Clean Json Project

**Json Project** ---> 
### Tecnologías

  * [nltk](https://www.nltk.org/)
  * [Postgres](https://www.postgresql.org/)

### Project Open Source

### Crear Base de Datos en postgres

  - `sudo su postgres`
  - `psql -c "DROP DATABASE txt_app"`
  - `psql -c "DROP USER txt_user"`
  - `psql -c "CREATE USER txt_user WITH ENCRYPTED PASSWORD 'XXXYYYZZZ'"`
  - `psql -c "CREATE DATABASE txt_app WITH OWNER txt_user"`

### Install Postgresql
  - sudo apt-get update
  - sudo apt-get install postgresql postgresql-contrib

### Create Virtualenv	
	- virtualenv -p python3 env

### Activate virtalenv env
	- source env/bin/activate

### Install requirements 
	- pip install -r requirements.txt

### Run project 

	- python main.py

### Para iniciar el servicio 
  - sudo service postgresql start

### para asignar contraseña 
  - sudo passwd postgres

### para entrar a postgres 
  - su postgres


### Agregar permisos a tabla
  - GRANT ALL PRIVILEGES ON TABLE dates TO txt_user;  


### create database

  - psql
  - \c txt_app; # Para usar la base de datos 
  - create table dates; # para crear la tabla dates
  - \d dates; # para ver el detalle de la base de datos

