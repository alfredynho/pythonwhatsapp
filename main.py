#!/usr/bin/python3

from means.read import read_text # Importando la funcion para leer los jsons
from prettytable import PrettyTable # Libreria para formatear el menu de opciones
import os # Importando OS para ejecutar comandos linux
from means.extract import names_json_files # Importacion de la funcion para sacar todos los nombres de los archivos json

# Formateando Menu
x = PrettyTable(["Opciones", "Comandos"])

# Cargando Opciones al Menu

x.add_row(["Cargar Conversaciones",0])
x.add_row(["Limpiar Conversaciones",1])
x.add_row(["Verifica Conversaciones (Positivos , Negativos)", 2])
x.add_row(["Cantidad de palabras Malas por conversacion", 3])
x.add_row(["Generar Matriz", 4])
x.add_row(["Salir", 5])

# Creando Menu de Opciones
def menu():
    # Limpiando la terminal
    os.system("clear")
    # Mostrando Menu de Opciones
    print('\033[1;32;40m' + '\033[1m' + x.get_string(title='>>>>>>         TRATA Y TRAFICO DE PERSONAS        <<<<<<' + '\033[1;32;40m'))


while True: # Siempre y cuando la condicion sea True

    menu()
    option = input("Ingrese su opcion ")

    # Opcion 0 Realiza el cargado de los archivos json
    if option == "0":
        print("")
        cont = 0
        json_list = names_json_files('json')
        for name_json in json_list:
            print('# ' , cont , name_json)
            cont +=1

        print("\n")
        input(" >>> Se realizo la limpieza de los Archivos \n... Presiona Enter para continuar ")        


    if option == "1":
        print("")
        read_text(1,'json')    

        print("\n")
        input(" >>> Se realizo la limpieza de los Archivos \n... Presiona Enter para continuar ")        
        
    # Opcion 2 Realiza la clasificacion de Archivos cada 3 meses
    elif option == "2":
        print("")
        read_text(2,'jsondb')

        print("\n")
        input(" >>> Calificacion y palabras malas cada 3 Meses \n... Presiona Enter para continuar ")        

    # Opcion para llevar todas las conversaciones a una sola linea
    elif option == "3":
        print("")
        read_text(3,'jsondb')

        print("\n")
        input(" >>> Cantidad de palabras Malas por Mes \n... Presiona Enter para continuar ")                

    # Opcion para sacar cantidad de palabras malas por conversacion y Generar Matriz
    elif option == "4":
        print("")
        # para llevar todas las conversaciones a una linea
        read_text(4,'jsonqualification')
        # funcion para generar matriz a partir de un solo archivo el matriz.json 
        read_text(5,'jsondb')

        print("\n")
        input(" >>> Se termino de generar la Matriz  \n... Presiona Enter para continuar ")                

    # Opcion para terminar el flujo
    elif option == "5":
        print("Bye ...")
        break

    else:
        print("")
        input("No ingresaste ninguna opcion \n presiona enter para continuar ")
